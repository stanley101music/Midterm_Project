// Initialize Firebase
var config = {
    apiKey: "AIzaSyDdQTWHW0R2Do0lwqndgMWhoNJIctEjLdw",
    authDomain: "midterm-project-105062229.firebaseapp.com",
    databaseURL: "https://midterm-project-105062229.firebaseio.com",
    projectId: "midterm-project-105062229",
    storageBucket: "midterm-project-105062229.appspot.com",
    messagingSenderId: "598126716023"
  };
  firebase.initializeApp(config);

// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
messaging.usePublicVapidKey('BL0xTNCifU4zPpD72oeuW2Y3W8hvLT4IHKmH-zuw0OzIMpp_1C4WgD2I2PLzrbdaWjddfr8heZBG8aH0aPMKm8o');

messaging.requestPermission()
.then(function() {
  console.log('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  return messaging.getToken();
  // ...
})
.then(function(token){
  console.log(token);
})
.catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});

messaging.onMessage(function(payload){
  console.log('onMessag: ',payload);
  appendMessage(payload);
});

function resetUI() {
  clearMessages();
  showToken('loading...');
  // [START get_token]
  // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken().then(function(currentToken) {
    if (currentToken) {
      sendTokenToServer(currentToken);
      updateUIForPushEnabled(currentToken);
    } else {
      // Show permission request.
      console.log('No Instance ID token available. Request permission to generate one.');
      // Show permission UI.
      updateUIForPushPermissionRequired();
      setTokenSentToServer(false);
    }
  }).catch(function(err) {
    console.log('An error occurred while retrieving token. ', err);
    showToken('Error retrieving Instance ID token. ', err);
    setTokenSentToServer(false);
  });
}

messaging.onTokenRefresh(function() {
  messaging.getToken()
  .then(function(refreshedToken) {
    console.log('Token refreshed.');
    // Indicate that the new Instance ID token has not yet been sent to the
    // app server.
    setTokenSentToServer(false);
    // Send Instance ID token to app server.
    sendTokenToServer(refreshedToken);
    // ...
  })
  .catch(function(err) {
    console.log('Unable to retrieve refreshed token ', err);
    showToken('Unable to retrieve refreshed token ', err);
  });
});

