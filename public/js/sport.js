function init() {
    var user_email = '';
    var uploadFileInput;
    var downloadURL;
    firebase.auth().onAuthStateChanged(function (user) {
        var user_status = document.getElementById('signin');
        // Check user login
        if (user) {
            user_email = user.email;
            user_status.innerHTML = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
            + user_email +'</a><div id="dynamic-menu" class="dropdown-menu" aria-labelledby="dropdown01"><a class="dropdown-item" id="logout-btn">Log out</a><a class="dropdown-item" id="profile">Profile</a></div></li>'
           console.log(user.email);
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(e) {
                firebase.auth().signOut().then(function(e){
                    alert("success");
                }).catch(function(e){
                    alert("error");
                });
                window.location.replace("index.html");
            });

            var btnProfile = document.getElementById('profile');
            btnProfile.addEventListener('click', function(e){
                window.location.replace("profile.html");
            });

            uploadFileInput = document.getElementById("uploadFileInput");
            var storageRef = firebase.storage().ref();
            
            uploadFileInput.addEventListener("change", function(){
                var file = this.files[0];
                var uploadTask = storageRef.child('images/'+ file.name).put(file);
                uploadTask.on('state_changed', function(snapshot){
                    // 觀察狀態變化，例如：progress, pause, and resume

                    // 取得檔案上傳狀態，並用數字顯示

                    var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    if(progress<100){
                        document.getElementById('post_btn').disabled=true;
                    }
                    else{
                        document.getElementById('post_btn').disabled=false;
                    }
                    console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                    case firebase.storage.TaskState.PAUSED: // or 'paused'

                        console.log('Upload is paused');
                        break;
                    case firebase.storage.TaskState.RUNNING: // or 'running'

                        console.log('Upload is running');
                         break;
                    }
                }, function(error) {
                    // Handle unsuccessful uploads
                    alert(error.message);
                }, function() {
                    // Handle successful uploads on complete

                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...

                    downloadURL = uploadTask.snapshot.downloadURL;
                    
                 });
            },false);


        } else {
            // It won't show any post if not login
            user_status.innerHTML = '<a class="nav-link" href="signin.html">Log in</a>';
            //document.getElementById('post_list').innerHTML = "";
        }
    });
    
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "" && uploadFileInput.files.length == 0) {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var listRef = firebase.database().ref('sport_list');
            listRef.push({email: firebase.auth().currentUser.email, data: post_txt.value, displayName: firebase.auth().currentUser.displayName, photoURL: firebase.auth().currentUser.photoURL, uploadImg: null}).catch(e=>console.log(e.message));
            post_txt.value="";
            console.log(1);
        }
        else if(post_txt.value!="" && uploadFileInput.files.length != 0){
            var listRef = firebase.database().ref('sport_list');
            listRef.push({email: firebase.auth().currentUser.email, data: post_txt.value, displayName: firebase.auth().currentUser.displayName, photoURL: firebase.auth().currentUser.photoURL, uploadImg: downloadURL});    
            post_txt.value="";
            uploadFileInput.value="";
            console.log(2);
        }
        else if(post_txt.value=="" && uploadFileInput.files.length !=0){
            var listRef = firebase.database().ref('sport_list');
            listRef.push({email: firebase.auth().currentUser.email, data: post_txt.value, displayName: firebase.auth().currentUser.displayName, photoURL: firebase.auth().currentUser.photoURL, uploadImg: downloadURL});    
            post_txt.value="";
            uploadFileInput.value="";
            console.log(3);
        }
        window.scrollTo(0,document.body.scrollHeight);
        //notifyMe();
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='";
    
    //img/default.jpg
    var str_after_picture = "' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 '><strong class='d-block text-gray-dark' style='float: left;'>";
    var str_after_content = "</p></div></div>\n";

    var imageURL = "";

    var postsRef = firebase.database().ref('sport_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html

            snapshot.forEach(function(childSnapshot){
                imageURL = "";
                var childData=childSnapshot.val();
                console.log(childData);
                var name;
                var picture;
                if(childData.displayName == null){
                    name = childData.email;
                }
                else{
                    name = childData.displayName;
                    console.log(childData.email);
                    console.log(firebase.auth().currentUser.email);
                    /*if (childData.email == firebase.auth().currentUser.email){
                        name = firebase.auth().currentUser.displayName;
                        console.log(name);
                        
                    }*/
                }
                if(childData.photoURL == null){
                    picture = 'img/default.jpg';
                    console.log(1);
                }
                else{
                    picture = childData.photoURL;
                    console.log(picture);
                    /*if(childData.email == firebase.auth().currentUser.email){
                        picture = firebase.auth().currentUser.photoURL;
                        
                    }*/
                }
                if(childData.uploadImg != null){
                    imageURL = childData.uploadImg;
                    toString(imageURL);
                    
                    imageURL = "<br><img src='" + imageURL + "' width='40%'>" ;
                    
                }
                
                
                total_post[total_post.length]=str_before_username + picture + str_after_picture +
                name + "</strong><br>" + childData.data + imageURL +
                str_after_content;
                first_count+=1;
            });
            document.getElementById('post_list').innerHTML = 
            total_post.join('');

            postsRef.on('child_added', function(data){
                imageURL = "";
                second_count+=1;
                if(second_count > first_count){
                    var childData=data.val();
                    var name;
                    if(childData.displayName == null){
                        name = childData.email;
                    }
                    else{
                        name = childData.displayName;
                        /*if (childData.email == firebase.auth().currentUser.email){
                            name = firebase.auth().currentUser.displayName;
                            console.log(name);
                            
                        }*/
                    }
                    if(childData.photoURL == null){
                        picture = 'img/default.jpg';
                    }
                    else{
                        picture = childData.photoURL;
                        /*if(childData.email == firebase.auth().currentUser.email){
                            picture = firebase.auth().currentUser.photoURL;
                            
                        }*/
                    }
                    if(childData.uploadImg != null){
                        imageURL = childData.uploadImg;
                        toString(imageURL);
                        console.log(12312312);
                    }
                    total_post[total_post.length]=str_before_username + picture + str_after_picture +
                    name + "</strong><br>" + childData.data + "<br><img src='" + imageURL + "' width='40%'>" +
                    str_after_content;
                    document.getElementById('post_list').innerHTML =
                    total_post.join('');

                    if(childData.email!=firebase.auth().currentUser.email){
                        notifyMe();
                    }
                }
            });

        })
        .catch(e => console.log(e.message));
}



window.onload = function () {
    init();
};


document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification("Stanley's Midterm Project", {
        icon: 'https://firebasestorage.googleapis.com/v0/b/midterm-project-105062229.appspot.com/o/images%2Ffirebase.png?alt=media&token=431833f7-da4a-497a-a998-ac26c6dd3f0f',
        body: "New message",
      });
  
      notification.onclick = function () {
        window.open("http://stackoverflow.com/a/13328397/1269037");   
        notification.close();   
      };
  
    }
  
  }