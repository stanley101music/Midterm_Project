function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        var user_status = document.getElementById('signin');
        var link_f = document.getElementById('food');
        var link_m = document.getElementById('music');
        var link_s = document.getElementById('sport');
        // Check user login
        if (user) {
            if(user.emailVerified){
                
                user_email = user.email;
                user_status.innerHTML = '<li class="nav-item dropdown"><a class="nav-link dropdown-toggle" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
                + user_email +'</a><div id="dynamic-menu" class="dropdown-menu" aria-labelledby="dropdown01"><a class="dropdown-item" id="logout-btn">Log out</a><a class="dropdown-item" id="profile">Profile</a></div></li>'
                /// TODO 5: Complete logout button event
                ///         1. Add a listener to logout button 
                ///         2. Show alert when logout success or error (use "then & catch" syntex)
                var btnLogout = document.getElementById('logout-btn');
                btnLogout.addEventListener('click', function(e) {
                    firebase.auth().signOut().then(function(e){
                        alert("success");
                    }).catch(function(e){
                        alert("error");
                    });
                });

                var btnProfile = document.getElementById('profile');
                btnProfile.addEventListener('click', function(e){
                    window.location.replace("profile.html");
                });
            
                user.providerData.forEach(function (profile) {
                    console.log("Sign-in provider: " + profile.providerId);
                    console.log("  Provider-specific UID: " + profile.uid);
                    console.log("  Name: " + profile.displayName);
                    console.log("  Email: " + profile.email);
                    console.log("  Photo URL: " + profile.photoURL);
                });

            
            
                link_f.innerHTML = '<a class="nav-link" href="food.html">Food</a>';
                link_m.innerHTML = '<a class="nav-link" href="music.html">Music</a>';
                link_s.innerHTML = '<a class="nav-link" href="sport.html">Sport</a>';
            }
            else{
                $(document).on('click',function(e){
                    if(e.target.id!="btn"){
                        var ans;
                        if(user.emailVerified)
                            ans = confirm("Verified");
                        else    
                            ans = confirm("Please verified email first");
                    if(ans){
                        location.reload();
                    }
                    else{
                        console.log(1);
                    }
                    }
                    
                });
                
                user.sendEmailVerification().then(function() {
                    console.log("驗證信已寄出");
                }, function(error) {
                    console.log("驗證信錯誤");
                });
                
            }
            
        } else {
            // It won't show any post if not login
            user_status.innerHTML = '<a class="nav-link" href="signin.html">Log in</a>';
            link_f.innerHTML = '<a class="nav-link disabled" href="#" id="F">Food</a>'
            link_m.innerHTML = '<a class="nav-link disabled" href="#" id="M">Music</a>'
            link_s.innerHTML = '<a class="nav-link disabled" href="#" id="S">Sport</a>'
            var f=document.getElementById("F");
            f.addEventListener('click',function(e){
                alert("Please sign in first!");
            });
            var m=document.getElementById("M");
            m.addEventListener('click',function(e){
                alert("Please sign in first!");
            });
            var s=document.getElementById("S");
            s.addEventListener('click',function(e){
                alert("Please sign in first!");
            });
            //document.getElementById('post_list').innerHTML = "";
        }
    });
    
        


    
    
    /*
    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field
            var listRef = firebase.database().ref('com_list');
            listRef.push({email: firebase.auth().currentUser.email, data: post_txt.value}).catch(e=>console.log(e.message));
            post_txt.value="";
        }
    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var postsRef = firebase.database().ref('com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. Join all post in list to html in once
            ///         4. Add listener for update the new post
            ///         5. Push new post's html to a list
            ///         6. Re-join all post in list to html when update
            ///
            ///         Hint: When history post count is less then new post count, update the new and refresh html
            
            snapshot.forEach(function(childSnapshot){
                var childData=childSnapshot.val();
                total_post[total_post.length]=str_before_username +
                childData.email + "</strong>" + childData.data +
                str_after_content;
                first_count+=1;
            });
            document.getElementById('post_list').innerHTML = 
            total_post.join('');

            postsRef.on('child_added', function(data){
                second_count+=1;
                if(second_count > first_count){
                    var childData=data.val();
                    total_post[total_post.length]=str_before_username
                    + childData.email + "</strong>" + childData.data +
                    str_after_content;
                    document.getElementById('post_list').innerHTML =
                    total_post.join('');
                }
            });

        })
        .catch(e => console.log(e.message));*/
}



window.onload = function () {
    init();
};

$(document).ready(function() {
    
    function snowFlakes(){
        var randomTime = Math.floor(Math.random() * (500) * 2);
        // Every randomTime call jquerysnow() and snowFlakes()
        setTimeout(function(){
            jquerysnow();
            snowFlakes();
        },randomTime);
    }
    
    function jquerysnow() {

        var snow = $('<div class="snow"></div>');
        //add new tag into index.html
        $('#snowflakes').prepend(snow);
        //snow x position randomly
        snowX = Math.floor(Math.random() * $('#snowflakes').width());        
        //set snow css position
        snow.css({'left':snowX+'px'});
        //set html
        snow.html("<img src='img/Snow.png'>");
        snow.animate({
            top: window.innerHeight,
            left: "+=50px",
            opacity : "1",
        }, 2000, "linear"/*fixed speed*/,function(){
            //delete itself
            $(this).remove();
        });
    }

    snowFlakes();
});


document.addEventListener('DOMContentLoaded', function () {
    if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
    }
  
    if (Notification.permission !== "granted")
      Notification.requestPermission();
  });
  
  function notifyMe() {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
      var notification = new Notification("Stanley's Midterm Project", {
        icon: 'https://firebasestorage.googleapis.com/v0/b/midterm-project-105062229.appspot.com/o/images%2Ffirebase.png?alt=media&token=431833f7-da4a-497a-a998-ac26c6dd3f0f',
        body: "New message",
      });
  
      notification.onclick = function () {
        window.open("http://stackoverflow.com/a/13328397/1269037");   
        notification.close();   
      };
  
    }
  
  }





